// respond to button click
console.log("Page load happened");

var sendButton = document.getElementById('send-button');
sendButton.onmouseup = getFormInfo;

function getFormInfo(){
    console.log("Entered get Form Info!")
    
    // select dropdown select-server-address
    var selindex = document.getElementById('select-server-address').selectedIndex;
    var url_base = document.getElementById('select-server-address').options[selindex].value;

    // text input-port-number
    var port_num = document.getElementById('input-port-number').value;

    // action from radio buttons
    var action = "GET"; // default
    if (document.getElementById('radio-get').checked){
        action = "GET";
    } 
    else if (document.getElementById('radio-put').checked) {
        action = "PUT";
    }
    else if (document.getElementById('radio-post').checked) {
        action = "POST";
    }
    else if (document.getElementById('radio-delete').checked) {
        action = "DELETE";
    }

    // define key/message
    var key = null;
    if (document.getElementById('checkbox-use-key').checked){
        key = document.getElementById('input-key').value;
    }
    var message_body = null;
    if (document.getElementById('checkbox-use-message').checked){
        // get key value
        message_body = document.getElementById('text-message-body').value;
    }

    makeRequest(url_base, port_num, action, key, message_body);

}

function makeRequest(url, port, action, key, message){
    console.log('entered make nw call');
    // set up url
    var xhr = new XMLHttpRequest(); // 1 - creating request object
    var full_url = url + ':' + port + '/movies/';
    
    // alter url based on key and message
    if (key){
        full_url = full_url + key;
    }
    
    xhr.open(action, full_url, true); // 2 - associates request attributes with xhr

    // set up onload
    xhr.onload = function(e) { // triggered when response is received
        // must be written before send
        console.log(xhr.responseText);
        // TODO: update the HTML with response text
        var answerLabel = document.getElementById("answer-label");
        answerLabel.innerHTML = xhr.responseText; 
    }

    // set up onerror
    xhr.onerror = function(e) { // triggered when error response is received and must be before send
        console.error(xhr.statusText);
    }

    // actually make the network call
    console.log(message);
    if (message){
        console.log("message exists");
        xhr.send(message); // last step - this actually makes the request
    }
    else {
        xhr.send(null);
    }
    
} // end of make nw call